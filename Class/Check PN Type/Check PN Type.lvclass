﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str"></Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">AOC.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../AOC.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Check PN Type</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"=35F.31QU+!!.-6E.$4%*76Q!!%LQ!!!27!!!!)!!!%JQ!!!!E!!!!!AF"4U-O&lt;(:M;7)61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!!!!I"=!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!#2%`1RHEUE3;@H\]HJ[@2N!!!!$!!!!"!!!!!!Z.?S6]M'!%7?-"AM=&lt;T72^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!.4:1R]TD\2"C=2\3O%/_2E"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1/G2G]!4[@,(8/L&amp;1`,:&amp;J1!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#%!!!!9?*RD9'.A;G#YQ!$%D!Z-$5Q:1.9(BA!'!$_B":9!!!!!!!!3!!!!#(C=9W"CY!"#"A!!6A!4!!!!!!"*!!!"'(C=9W$!"0_"!%AR-D!QX1$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XMM+%A?\?![3:1(*1.2Q1+;9,1(Q#X2R_+0U!31Q!F%-J(1!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!';!!!$;(C=W]$)Q*"J&lt;'('Q-4!Q!RECT-U-#4HJ[4S-A$Z$"#A!W.1!!+AZGGBC2M?/*Q'"(L]]CVA@P-&lt;HGY8&amp;9(G'B5*JF+2&lt;B]6E5Y@&amp;::/&amp;J58@`\``^^]B/&gt;QNU@/=5=&lt;E.JO$K$Y=2=6$B!(3,/![0_"'3"6K/&lt;*&gt;!*FA&lt;1%EA;YA3DW"Q"6=425+$/5M"A?C$J]P-'%%?*1G"/CM,G8?0/&lt;XX!!036Q]#&amp;,&gt;[-'E.]\%51#B8A[1TAEDLNQ[)A"_9QH1!:W]M"]T1(X4RD)A")6A5Y4E%5MD$#,ONG//WC!Q]&amp;""%*F1+A+#&amp;5!IH;!88#%)_YQ00T8PL[XCR6)MS(&amp;C1-1.Y!94+B9DY'2A2(-:'29#V6L!W1T1=6A=1NC+U#$D:("(K\H.F2?!]E=&amp;U;9(I3[;C2X-)(.9'4YQQ!T$WA@6%]$V.UA-6_AW!%I/Q4)HA"F2Q0:([$M*#"&lt;!-L/",).'#(M0#A&lt;&lt;"E$&lt;NL:X]56+:D!_1+7.&lt;C"/$GXQ-"!LTJ9*\C7A1P%,UAOAX):!-8/E(E!!!!!!,A!!!%9?*RT9'"AS$3W-.M!J*E:'2D%'2I9EP.45BG1Q!='X#!]L0G.1(?*CERHC1J0&gt;YW+1G?.#E=X7T&gt;`JY]+3S?,SIM`````&lt;`\"S$`F1+F"LRN,JYM+4[]\C/,I&gt;G0J^G@"5(?1@^N*@J=4H3&gt;?-`.P/^"]2#!O`D$#PL7P\_U#/J/"%=E.$E#MR#$"Q!3E16A2+AZC?S+J!1&amp;H@R&gt;8&gt;,_"T/)'YO4=!A-$P?JAH?";"A!%C$G*!!!!HA!!!0BYH(.A9'$).,9QGQ#EG2E:'-1:'BC3]V.3':$!"1&lt;=)$SM_9V+&gt;YW+17?.CE*XC9J*:YG+4$=\E'4J:&amp;&amp;Z]?@```_N"UK6?NV:/FV5&amp;*K0M9$FX6F1&amp;@#\('A_IB)8@ZBB\?N\OY$/9'"%MM-"C,/!)ER!'I4FI?*-5$E'*.L:X]56X?UAM\C!/,EAO5SP/FAHO*;"!1"#+4$&gt;!!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!(BY!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!(CMKKOM?!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!(CMKK/DI[/LL(A!!!!!!!!!!!!!!!!!!!!!``]!!(CMKK/DI[/DI[/DK[RY!!!!!!!!!!!!!!!!!!$``Q#LKK/DI[/DI[/DI[/DI[OM!!!!!!!!!!!!!!!!!0``!+KKI[/DI[/DI[/DI[/D`KM!!!!!!!!!!!!!!!!!``]!KKOLKK/DI[/DI[/D`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[KDI[/D`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OKL0\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+OLK[OLK[OL`P\_`P\_K[M!!!!!!!!!!!!!!!!!``]!!+3KK[OLK[P_`P\_K[SE!!!!!!!!!!!!!!!!!!$``Q!!!!#EK[OLK`\_K[OE!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!J+OLK[OD!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!+3D!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!$?1!!#/&gt;YH,V745]4523^&lt;SDG&amp;34-!!K.EB9SL=2!.#1+%D^!2B%F2&amp;*)KBOJNCI*C,;5G*$AVUD#AIUG83AGLH$JAI6&lt;.&lt;7;T!*8'M'E!8_!&lt;ACG-KXXP?GUJ&gt;C3'%)8,S_4&gt;_ZZ^ZRTJQ.!@YAV1A)G&gt;3$C+GZ[&gt;#DR;11AUE1B^8..A4B!`A#JMB%&gt;WOG!O#1ES(Y&gt;&gt;PMU&amp;TWM4--P0*V=3%\!(+E8F`(I,N''R5JU+0&gt;JV&gt;*Z/3&lt;+L`@,U]6G61HMYAR*#"\:PE:HV&amp;%E",7"L6)431"2[CQ7N@[C^Z:@F&gt;F4;R/V]:*7(52&amp;+QP)M1.9%;H@]Z)E$&amp;(SSCQ*7,)"ZO@H-S$*!,HY.6I:*IJFPZ*Q!5S&amp;IN5'Z6ADRZ2Q$0*].8E5?_)JOTM$Z5)L&amp;5V%+/,OGCV(43\&amp;LL&gt;TX-L+#O*Q4?&amp;O[F!FRUZ4'VXW,2;8^;FPA!#*8'(30G&gt;YK:?ZQ-^7I"(/.C+[=?`7Y;#K#9.A-&lt;HOQ#SXQ7,;="RN)'?Z$6:F7I3-$\.&lt;_+"9)P7VH=/BY*A`Y"C^\LAW\!U'(&lt;=$1_0?-&lt;`$ZRXT&lt;H&lt;IB+*:7VDXD)S(!`;!"5&lt;A:L&lt;;IT!X.Y=#Y*K"HE2IN2R,YS4?T8";=6^'==;;5?Y5+K@]\L`"V(/W#7:G":\:$LY?SC3X':-LC&lt;G8WZ$=)^O@X+-9K-G=Z))00"!KE-)7!Z368!_CBM"8!./+G(M&lt;EON"HC'4JV"SDWV/LM@E3C=X(!ZPQ+(_&lt;?HE7AAREKN_4KYHVVF_Y]EY8)=!T[`!%8VI"[O`$_NX9@U.UN`0G^&lt;_QNI\&gt;I'2W$+X0T$E(8&lt;UBE;O_A-Z4J4K]%$2&lt;(DD=I/!-I);$)%-*&gt;D&gt;N]8PW&amp;VI[K6E.W&lt;)[(XN%]/H^B_T^N'M`1&gt;KM^IT0!].HH'$Z[U:NA&lt;/AQT&lt;QI/+0=+R;::DJ6E46]VZ"EXHEB*X,B[0)S?OBG*K(&gt;T(P-LM38:3V:QRL%4^'D-RS$_'DT%'TEPAP!QYD"_=%Y)RDU7OK;,5)83_A`V&gt;`%4=PFTHO`)[@W%,ZY75][58P9'RP,[@WS(@OX@)^`0&lt;\0O&amp;``7^Z^__7\DPVN[M&gt;R6&gt;CI&lt;3HR&lt;5,3P*1A@%-VD`HAZ\F-[+"&lt;%&lt;^&gt;J.O_E:]17_R`'6PJ?_I+O2,_G0DUB0'KF_BH&gt;&amp;4_2H&gt;FLT&amp;VVJE;5!!!!!!!!%!!!!-A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!,!!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"O&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"3!!!!!Q!51$$`````#V"B=H1A4H6N9G6S!":!-0````].5W6S;7&amp;M)%ZV&lt;7*F=A!A1&amp;!!!A!!!!%61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!!%!!A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!N&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!#!!!!!!!!!!%!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.@2_^Y!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!V^(\XA!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"O&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"3!!!!!Q!51$$`````#V"B=H1A4H6N9G6S!":!-0````].5W6S;7&amp;M)%ZV&lt;7*F=A!A1&amp;!!!A!!!!%61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!!%!!A!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!#!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!"C&amp;Q#!!!!!!!-!&amp;%!Q`````QN198*U)%ZV&lt;7*F=A!71$$`````$6.F=GFB&lt;#"/&gt;7VC:8)!)%"1!!)!!!!"&amp;5.I:7.L)&amp;"/)&amp;2Z='5O&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!!!!!!!!!!!!%!!1!#A!!!!1!!!#&amp;!!!!+!!!!!)!!!1!!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%&lt;!!!"PXC=D6'R4M-Q%(X",7U3%FJISY4EE1%B*(YA+"*DC9!0Q%VMC$"+Z$A6&lt;0V&amp;*D;_!79'O#3N%',"4\:V\_\?/]M!$D#*4L^I_9EQFM`LRY5UG(6==#V.,P3'6@BY@@F]!]$=]]PY2#^VPJD'^T*^Y-G=XTS8ELB5C[I;`W:4KX%=*&gt;CC8O=QVH6FJ?'&amp;YGUR,UW_&amp;&amp;&lt;S4&amp;C"0A%?-KJ]"[/!L5/=5?"'!T#F\T#-6KSYMBUB#Y52[4---)4,[ET2X&lt;Y"9?PLQW@W+3/F*P+QA[.`TU.6@7QXBQ/3P;7G(CZ)C'-8)T*N-'Y(\]$7_-P]:0:;&gt;!^TM%_+W(S&amp;6ZICKV0,=UI':.]D^Q!B*JBC2DMEU#D@U6";M!!!!!#-!!%!!A!$!!1!!!")!!]!!!!!!!]!\1$D!!!!8A!0!!!!!!!0!/U!YQ!!!(1!$Q!!!!!!$Q$N!/-!!!#+A!#!!)!!!!]!\1$D&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*!4"35V*$$1I!!UR71U.-1F:8!!!3P!!!"&amp;9!!!!A!!!3H!!!!!!!!!!!!!!!)!!!!$1!!!2%!!!!'UR*1EY!!!!!!!!"6%R75V)!!!!!!!!";&amp;*55U=!!!!!!!!"@%.$5V1!!!!!!!!"E%R*&gt;GE!!!!!!!!"J%.04F!!!!!!!!!"O&amp;2./$!!!!!"!!!"T%2'2&amp;-!!!!!!!!"^%R*:(-!!!!!!!!##&amp;:*1U1!!!!#!!!#((:F=H-!!!!%!!!#7&amp;.$5V)!!!!!!!!#P%&gt;$5&amp;)!!!!!!!!#U%F$4UY!!!!!!!!#Z'FD&lt;$A!!!!!!!!#_%R*:H!!!!!!!!!$$%:13')!!!!!!!!$)%:15U5!!!!!!!!$.&amp;:12&amp;!!!!!!!!!$3%R*9G1!!!!!!!!$8%*%3')!!!!!!!!$=%*%5U5!!!!!!!!$B&amp;:*6&amp;-!!!!!!!!$G%253&amp;!!!!!!!!!$L%V6351!!!!!!!!$Q%B*5V1!!!!!!!!$V&amp;:$6&amp;!!!!!!!!!$[%:515)!!!!!!!!$`!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!/!!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!%5!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!P````]!!!!!!!!"2!!!!!!!!!!!`````Q!!!!!!!!&amp;=!!!!!!!!!!$`````!!!!!!!!!;Q!!!!!!!!!!0````]!!!!!!!!"P!!!!!!!!!!"`````Q!!!!!!!!.=!!!!!!!!!!,`````!!!!!!!!""A!!!!!!!!!"0````]!!!!!!!!%P!!!!!!!!!!(`````Q!!!!!!!!41!!!!!!!!!!D`````!!!!!!!!"/!!!!!!!!!!#@````]!!!!!!!!%^!!!!!!!!!!+`````Q!!!!!!!!5%!!!!!!!!!!$`````!!!!!!!!"2A!!!!!!!!!!0````]!!!!!!!!&amp;-!!!!!!!!!!!`````Q!!!!!!!!6%!!!!!!!!!!$`````!!!!!!!!"=A!!!!!!!!!!0````]!!!!!!!!*T!!!!!!!!!!!`````Q!!!!!!!!H=!!!!!!!!!!$`````!!!!!!!!$6Q!!!!!!!!!!0````]!!!!!!!!.:!!!!!!!!!!!`````Q!!!!!!!!VM!!!!!!!!!!$`````!!!!!!!!$8Q!!!!!!!!!!0````]!!!!!!!!.Z!!!!!!!!!!!`````Q!!!!!!!!XM!!!!!!!!!!$`````!!!!!!!!%,!!!!!!!!!!!0````]!!!!!!!!1O!!!!!!!!!!!`````Q!!!!!!!"$!!!!!!!!!!!$`````!!!!!!!!%/Q!!!!!!!!!A0````]!!!!!!!!3$!!!!!!21WBF9WMA5%YA6(FQ:3ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AF"4U-O&lt;(:M;7)61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!Q!"!!!!!!!!!!!!!!%!(%"1!!!61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!!%!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!P``!!!!!1!!!!!!!1!!!!!"!"R!5!!!&amp;5.I:7.L)&amp;"/)&amp;2Z='5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!)*15^$,GRW&lt;'FC#U&amp;01SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!1!!!!!!!A!!!!!$!"2!-0````],5'&amp;S&gt;#"/&gt;7VC:8)!&amp;E!Q`````QV4:8*J97QA4H6N9G6S!'9!]&gt;@2_^Y!!!!$#5&amp;01SZM&gt;GRJ9B6$;'6D;S"14C"5?8"F,GRW9WRB=X-21WBF9WMA5%YA6(FQ:3ZD&gt;'Q!,%"1!!)!!!!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!,``````````Q!!!!!!!!!!!!!!!!!!!AF"4U-O&lt;(:M;7),15^$,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!V!!!!!AF"4U-O&lt;(:M;7),15^$,GRW9WRB=X.16%AQ!!!!%Q!"!!1!!!!,15^$,GRW9WRB=X-!!!!!</Property>
	<Item Name="Check PN Type.ctl" Type="Class Private Data" URL="Check PN Type.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="Part Number" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Part Number</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Part Number</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Part Number.vi" Type="VI" URL="../Accessor/Part Number Property/Read Part Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],5'&amp;S&gt;#"/&gt;7VC:8)!0%"Q!"Y!!#%*15^$,GRW&lt;'FC&amp;5.I:7.L)&amp;"/)&amp;2Z='5O&lt;(:D&lt;'&amp;T=Q!21WBF9WMA5%YA6(FQ:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!)1F"4U-O&lt;(:M;7)61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!""$;'6D;S"14C"5?8"F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Part Number.vi" Type="VI" URL="../Accessor/Part Number Property/Write Part Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!B#5&amp;01SZM&gt;GRJ9B6$;'6D;S"14C"5?8"F,GRW9WRB=X-!%5.I:7.L)&amp;"/)&amp;2Z='5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;%!Q`````QN198*U)%ZV&lt;7*F=A!]1(!!(A!!)1F"4U-O&lt;(:M;7)61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!""$;'6D;S"14C"5?8"F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Serial Number" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Serial Number</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Serial Number</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Serial Number.vi" Type="VI" URL="../Accessor/Serial Number Property/Read Serial Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].5W6S;7&amp;M)%ZV&lt;7*F=A!]1(!!(A!!)1F"4U-O&lt;(:M;7)61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!"&amp;$;'6D;S"14C"5?8"F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!B#5&amp;01SZM&gt;GRJ9B6$;'6D;S"14C"5?8"F,GRW9WRB=X-!%%.I:7.L)&amp;"/)&amp;2Z='5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Serial Number.vi" Type="VI" URL="../Accessor/Serial Number Property/Write Serial Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!B#5&amp;01SZM&gt;GRJ9B6$;'6D;S"14C"5?8"F,GRW9WRB=X-!%5.I:7.L)&amp;"/)&amp;2Z='5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!Q`````QV4:8*J97QA4H6N9G6S!$R!=!!?!!!B#5&amp;01SZM&gt;GRJ9B6$;'6D;S"14C"5?8"F,GRW9WRB=X-!%%.I:7.L)&amp;"/)&amp;2Z='5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!B!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Override" Type="Folder">
		<Item Name="Do.vi" Type="VI" URL="../Do.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!B#5&amp;01SZM&gt;GRJ9B6$;'6D;S"14C"5?8"F,GRW9WRB=X-!%5.I:7.L)&amp;"/)&amp;2Z='5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!#%*15^$,GRW&lt;'FC&amp;5.I:7.L)&amp;"/)&amp;2Z='5O&lt;(:D&lt;'&amp;T=Q!11WBF9WMA5%YA6(FQ:3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="Is Luxshare PN.vi" Type="VI" URL="../Is Luxshare PN.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!)1Z*=S"-&gt;8BT;'&amp;S:3"14A!!0%"Q!"Y!!#%*15^$,GRW&lt;'FC&amp;5.I:7.L)&amp;"/)&amp;2Z='5O&lt;(:D&lt;'&amp;T=Q!21WBF9WMA5%YA6(FQ:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!)1F"4U-O&lt;(:M;7)61WBF9WMA5%YA6(FQ:3ZM&gt;G.M98.T!""$;'6D;S"14C"5?8"F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
</LVClass>
