﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">40813</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">11507131</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str"></Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">AOC.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../../../AOC.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,)!!!*Q(C=T:1^DBJ"%)5@FK6VSAH7YA:7J1[Z!F?I+R$OB*T!5FX!!3+U.I(%=I9KA'1T*0M#R/M%`-V-,\],/,#F\;:GBP?KKT_[GZ';^E&amp;[L_WN^O.K?`&amp;6@[BZ_(4ONV:\PTR?&amp;`X&gt;Z=RP"_XG0`=P]N^KTV@&lt;K4_&lt;8@.HC`FC-&lt;PEY^:)2RF\P\AH'4N`\\9:J`\]S&amp;_?_YN^B?6S/8`&amp;,RH,Z57@D-8SKF`MN_=PDPT&amp;9D+:(+``@,\T_:G4JBXPX\TV$^T4`7]S@ETW^NHZG2_ZZT\NQ(X60WIXDT@`G?LWH_$`.KGD_E6%*"'%%[&lt;;WC:[IC&gt;[IC&gt;[IA&gt;[I!&gt;[I!&gt;[I$O[ITO[ITO[IRO[I2O[I2O[I4=&gt;8?B#&amp;TKLEES?4*1541IES;!IO31]#5`#E`$Q61F0QJ0Q*$Q*$SF+?"+?B#@B38A9JI1HY5FY%J[%BV).349&gt;(:[%B`)+?!+?A#@A#8C95A&amp;0!""-&amp;B1/CI#BQ!Q?!J[!*_$B51&amp;0Q"0Q"$Q"$\9#HI!HY!FY!B['.+M3$5X&gt;U?'BD"Q?B]@B=8A=(EL,Y8&amp;Y("[(R_&amp;B/DE]$I]$Y5TI&amp;!&gt;"TC!HQ@HC]$A]X/4Q/$Q/D]0D]'!V/_4.SN1U&gt;5?(R_!R?!Q?A]@AI91-(I0(Y$&amp;Y$"\+SO!R?!Q?A]@A93I:0!;0Q7/!'*-SP9RCRE!DS2!-(D\.;&lt;&amp;GF[)BM;:,Z@#K(%K6Q[:SC&amp;1/B]KGKWSGSC;J,,\+IKIMFMICK0QY&amp;7A6'*6*6!&lt;8C6JT82&amp;49ES-C#%R)0J%D_D71`^RYHK^VGKVUH1[V8A]VGAUUH!YV'!Q5,`@6[`85\@&lt;V5P\3'^&lt;Z`#^N&amp;F_?^A]@?(_&gt;&lt;NZ?NRO0TXOXBU6O68&gt;\XZWKPPH4P7Q[4$Q&lt;_.8^&lt;$^8&gt;VTP^N_LTJI_NSJ;\&lt;PJ&lt;@Q&lt;N1\N@@&gt;'PU"JS1=6Q!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">UBNT</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"065F.31QU+!!.-6E.$4%*76Q!!$[1!!!1R!!!!)!!!$Y1!!!!&lt;!!!!!AF"4U-O&lt;(:M;7)-65*/6#ZM&gt;G.M98.T!!!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!M9Q"2_GY$57.DQ^JM/=IM1!!!!Q!!!!1!!!!!(NRRB&amp;T0$&amp;&amp;P86)&lt;TUY1/T5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!"ZWQAQ-4O/3I[$L)U-JG*.!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%/&gt;D#.#,@KEB8.&lt;;+N&amp;E8'E!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!C!!!!((C=9W"D9'JAO-!!R)R!T.4!^!0)`A$C-Q!!;!%).A!!!!!!21!!!2BYH'.AQ!4`A1")-4)Q-*U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"SK8J"`Q(Z#!7R9T!9!&gt;P5I&amp;1!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!&amp;E!!!#T(C=3W"E9-AUND$&lt;!+3:A6C=I9%B/4]FF9M"S'?!A#V-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#N8_(3$JQ]DOY1![1O$A1Z&lt;O2AUAPX=CC!1+]83'=%A=&gt;_(1%10S'5^U!KXPZ)'ZEA.O@RD)A")6A5Y4E%N:1+;$V83T(8@1!,P&lt;112#:5#I#AB6!(9-W!6(//)/Q].L\?N\OU$BS)95BAZ!X!$%I$B%RHI-D!QA#ZG!:#V5L1W1T116A]5&amp;C(U"SN:!UP-&amp;S8S1(J$-'KA9C,U*SG[!OA=E^B&gt;)4Y#S1&lt;Z.A,+ZA?Q&amp;5,91E#U!:5M#W1_A&lt;$EI?Q-UCH$2TPYOLED?B[&gt;0!$5&gt;=I!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0```````````N'U@`\7FP`_U;&lt;``N;W``]RNP```````````Y!!!!'!!9!"A!@A!9!@_!'!@`Y"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``_'!@``ZA"``Y9!(`]'!!@]"A!!]!9!!!!(`````!!!#!0`````````````````````_\O\O\O\O\O\O\O\O\O\P`O\OY/Y/!!\A\AY!$O\O\`\O\O$O$A\AY!Y/Y/\O\O`_\O\A\AY!$O$A$O$O\O\P`O\OY/Y/$O$A\A\A\O\O\`\O\OY!\A!/Y/Y/Y/\O\O`_\O\O\O\O\O\O\O\O\O\P``````````````````````T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-TOT-T-T-T-T0`-T-T-T-TPH`\-T-T-T-T`T-T-T-TPHO\P`MT-T-T-`]T-T-TPHO\O\O`_T-T-T0`-T-T0HO\O\O\O\`T-T-T`T-T-S:\O\O\O\O`]T-T-`]T-T-H`HO\O\O``H-T-T0`-T-T*``_?\O```ZT-T-T`T-T-S@```Z````_=T-T-`]T-T-H`````````H-T-T0`-T-T*`````````ZT-T-T`T-T-S@````````_=T-T-`]T-T-H`````````H-T-T0`-T-T*`````````ZT-T-T`T-T-T````````````]T-`]T-T-S:```````Z````T0`-T-T-T*`````Z````T-T`T-T-T-T-H``_`````-T-`]T-T-T-T-S?`````-T-T0`-T-T-T-T-T-```-T-T-T`T-T-T-T-T-T-T-T-T-T-``````````````````````!!!%!0```````````````````````````````````````````]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q```Q]0$Q]0$!-0$!--!!!$$QQ$$QQ$$!!!!Q]0$Q]0$```$Q]0$Q]-!Q]-!QQ$$QQ$$!!$$!-0$!-0$Q]0$Q]0``]0$Q]0$QQ$$QQ$$!!!!Q]-!QQ!!Q]-!Q]0$Q]0$Q```Q]0$Q]0$!-0$!--!Q]-!QQ$$QQ$$QQ$$Q]0$Q]0$```$Q]0$Q]0$!!$$QQ!!!-0$!-0$!-0$!-0$Q]0$Q]0``]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q]0$Q``````````````````````````````````````````````Y_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0D```DY_0DY_0DY_0DY_0DYP,TY_0DY_0DY_0DY_0DY_0``_0DY_0DY_0DY_0DYP.$+U.#]_0DY_0DY_0DY_0DY```Y_0DY_0DY_0DYP.$+Q]0$Q^$1P0DY_0DY_0DY_0D```DY_0DY_0DYP.$+Q]0$Q]0$Q]01U,TY_0DY_0DY_0``_0DY_0DY_.$+Q]0$Q]0$Q]0$Q]0$U.$Y_0DY_0DY```Y_0DY_0DYSML$Q]0$Q]0$Q]0$Q]02U0DY_0DY_0D```DY_0DY_0D+U.$+Q]0$Q]0$Q]02U&gt;(+_0DY_0DY_0``_0DY_0DY_-L1U.$1SM0$Q]02U&gt;(2U=LY_0DY_0DY```Y_0DY_0DYSN$1U.$1U-L1U&gt;(2U&gt;(2SPDY_0DY_0D```DY_0DY_0D+U.$1U.$1U.(2U&gt;(2U&gt;(+_0DY_0DY_0``_0DY_0DY_-L1U.$1U.$1U&gt;(2U&gt;(2U=LY_0DY_0DY```Y_0DY_0DYSN$1U.$1U.$2U&gt;(2U&gt;(2SPDY_0DY_0D```DY_0DY_0D+U.$1U.$1U.(2U&gt;(2U&gt;(+_0DY_0DY_0``_0DY_0DY_-L1U.$1U.$1U&gt;(2U&gt;(2U=LY_0DY_0DY```Y_0DY_0DYU.$1U.$1U.$2U&gt;(2U&gt;(1U+SML0DY_0D```DY_0DY_0DYSML1U.$1U.(2U&gt;(1U-KML+SML+TY_0``_0DY_0DY_0DY_-L1U.$1U&gt;(1U-KML+SML+TY_0DY```Y_0DY_0DY_0DY_0D+U.$1U-/ML+SML+SM_0DY_0D```DY_0DY_0DY_0DY_0DYSM/ML+SML+SM_0DY_0DY_0``_0DY_0DY_0DY_0DY_0DY_0CML+SM_0DY_0DY_0DY```Y_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0D```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!#;1!!"/*YH+W5TWM4124(XY26*K(&amp;W446,L1E,&gt;.;*,HYMQ;DJ:U?+K5IM&gt;"$Q19X`I$7;*.54_VF%8)I&amp;)1=B"[]Z/IB"_]3P/R"S='$":@_"V[+ANX%.ZPM&lt;B)R8ERA'*,X?&gt;_X\`NF!:2P&lt;#41A"U&lt;#$P'SZ).)&gt;UE!,5%B@:H[B7Q&amp;@),3%1D.MT3&amp;899;*"2'Q:U=YJ?&amp;C8YXKYG+@+7(7(J;;:BMZ!.:X4TH(K&lt;7YS`'_7F5WZ8&amp;=&lt;:(GE%6HHU"^UT=CA)RL1]V12J!"(DCG*-X-E]T2J=`BJ-5-VJ'&lt;3"#8.QGVPHM3.+@X";EHU32_FW3]#7UV#N6HV)&lt;5&amp;4TBATS*#Y-_Z_(S;-T!NOR2UGZ$#IEX*V2,4R7MYOI6ZUC*NZ&lt;C(WUNV-X*5358P7Q?LV/G*YNL((.E3Y.5=V?K2`*9.XD@&gt;!A.4O?T[IS^)%JT;-0EQG#5PD07X$"=--L$0&amp;X7Y2+IY,COP#$?H#H/.#5*19_$:5`GF$&lt;7*M@K/9,W3X9LG(M1=&lt;G8Q_^GTLS8;GE)XJG5,G4Y.3QN1OS;?89KVM$)-#TW'^=^EZK&amp;1KO!!]@@1GIE0=]DD6?2L&gt;7\DO,VSK_JO\B:M40_]^ENO&lt;4!&lt;=S))@VIM96D83/V"87+`]`\"?&amp;?&lt;94E^991W`GXW#&gt;[U&amp;_7(&amp;?I".7/P$T##TWRH7FIKLUS_MVY8*ON-KW:[UFMPF,AZXHP43KB$33KPRO8H30/H.,!38/S;HBR_,XP]UT574;H]P9!MINGP$M*A0@W+,W'W!,N)&amp;&gt;I#OYNPI,$WAR\5P8HFNS&lt;PS.V%[]BOK/&amp;:=!!!!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!!L!!!!!)!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!/"=!A!!!!!!"!!A!-0````]!!1!!!!!!(!!!!!%!&amp;%"1!!!-65*/6#ZM&gt;G.M98.T!!!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!%!!)!#!!!!!1!!!"!!!!!+!!!!!)!!!1!!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$@!!!"5XC=D9_^4M.!%)3`SQ6CZQ=#K:'OI%C6BB=QC51*&amp;I)?*\:2J*/-YEN%"RX0S&amp;-!&lt;]$%#;+A13/N&gt;G:P:`;!%3F@\S]@&lt;Y#.,W^G%\`RSXH`@HJ^JX&lt;BM\K/'L))HH'3QNH-L_N1L&amp;R6OG&lt;OHF&lt;,429+FW=BYU#A3Q\G%S.C^Z1,N8(3Q:&lt;_E3BZN&gt;6NW!F&amp;64+5N^8T1TJWH:&gt;UNVEW0/@;4GE2%80_L`Q7D9W+17M0CGZT*2.(HY(%,9Y5^A/TRV`F&gt;X,=90=2QV#/[#!5VJ&lt;;YY24V&gt;YXC-AT\A!!!!#-!!%!!A!$!!1!!!")!!]!!!!!!!]!\1$D!!!!8A!0!!!!!!!0!/U!YQ!!!(1!$Q!!!!!!$Q$N!/-!!!#+A!#!!!0I!!]!^Q$J&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*!4"35V*$$1I!!UR71U.-1F:8!!!0J!!!"$%!!!!A!!!0B!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!`````Q!!!!!!!!$%!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!%-!!!!!!!!!!$`````!!!!!!!!!21!!!!!!!!!!0````]!!!!!!!!"0!!!!!!!!!!!`````Q!!!!!!!!')!!!!!!!!!!$`````!!!!!!!!!:A!!!!!!!!!"0````]!!!!!!!!$!!!!!!!!!!!(`````Q!!!!!!!!-5!!!!!!!!!!D`````!!!!!!!!!S1!!!!!!!!!#@````]!!!!!!!!$/!!!!!!!!!!+`````Q!!!!!!!!.)!!!!!!!!!!$`````!!!!!!!!!VQ!!!!!!!!!!0````]!!!!!!!!$&gt;!!!!!!!!!!!`````Q!!!!!!!!/)!!!!!!!!!!$`````!!!!!!!!"!Q!!!!!!!!!!0````]!!!!!!!!'%!!!!!!!!!!!`````Q!!!!!!!!I5!!!!!!!!!!$`````!!!!!!!!#C1!!!!!!!!!!0````]!!!!!!!!-F!!!!!!!!!!!`````Q!!!!!!!!S=!!!!!!!!!!$`````!!!!!!!!$+1!!!!!!!!!!0````]!!!!!!!!-N!!!!!!!!!!!`````Q!!!!!!!!U=!!!!!!!!!!$`````!!!!!!!!$31!!!!!!!!!!0````]!!!!!!!!.V!!!!!!!!!!!`````Q!!!!!!!!X=!!!!!!!!!!$`````!!!!!!!!$?1!!!!!!!!!!0````]!!!!!!!!/%!!!!!!!!!#!`````Q!!!!!!!!\U!!!!!!B61EZ5,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AF"4U-O&lt;(:M;7)-65*/6#ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!!!!)!!1!!!!!!!!!!!!!"!"2!5!!!$&amp;6#4F1O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!"``]!!!!"!!!!!!!"!!!!!!%!&amp;%"1!!!-65*/6#ZM&gt;G.M98.T!!!"!!!!!!!"`````A!!!!!!!!)*15^$,GRW&lt;'FC&amp;%RV?(.I98*F,5F$6#ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"5!!!!!AF"4U-O&lt;(:M;7)54(6Y=WBB=G5N35.5,GRW9WRB=X-!5&amp;2)-!!!!#A!!1!%!!!-4(6Y=WBB=G5N35.5&amp;%RV?(.I98*F,5F$6#ZM&gt;G.M98.T!!!!!!</Property>
	<Item Name="UBNT.ctl" Type="Class Private Data" URL="UBNT.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Get Serial Number.vi" Type="VI" URL="../Get Serial Number.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!9#5&amp;01SZM&gt;GRJ9AR61EZ5,GRW9WRB=X-!!!B61EZ5)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!'!F"4U-O&lt;(:M;7)-65*/6#ZM&gt;G.M98.T!!!(65*/6#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
		</Item>
		<Item Name="Get Length.vi" Type="VI" URL="../Get Length.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!9#5&amp;01SZM&gt;GRJ9AR61EZ5,GRW9WRB=X-!!!B61EZ5)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!'!F"4U-O&lt;(:M;7)-65*/6#ZM&gt;G.M98.T!!!(65*/6#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
		</Item>
		<Item Name="Get Length (OM2).vi" Type="VI" URL="../Get Length (OM2).vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!9#5&amp;01SZM&gt;GRJ9AR61EZ5,GRW9WRB=X-!!!B61EZ5)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!'!F"4U-O&lt;(:M;7)-65*/6#ZM&gt;G.M98.T!!!(65*/6#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Get Date Code.vi" Type="VI" URL="../Get Date Code.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!9#5&amp;01SZM&gt;GRJ9AR61EZ5,GRW9WRB=X-!!!B61EZ5)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!'!F"4U-O&lt;(:M;7)-65*/6#ZM&gt;G.M98.T!!!(65*/6#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
		</Item>
	</Item>
</LVClass>
