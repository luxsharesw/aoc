﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Class" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Public Class" Type="Folder">
			<Item Name="Check Pass Count" Type="Folder">
				<Item Name="Check Pass Success.lvclass" Type="LVClass" URL="../Class/Check EEPROM Success/Check Pass Success.lvclass"/>
			</Item>
			<Item Name="Search Product" Type="Folder">
				<Item Name="Search Product.lvclass" Type="LVClass" URL="../Class/Search Product/Search Product.lvclass"/>
			</Item>
			<Item Name="Search Product By Identifier" Type="Folder">
				<Item Name="Search Product By Identifier.lvclass" Type="LVClass" URL="../Class/Search Product By Identifier/Search Product By Identifier.lvclass"/>
			</Item>
			<Item Name="Search QSFP28 Fanout Gen2" Type="Folder">
				<Item Name="Search QSFP28 Fanout Gen2.lvclass" Type="LVClass" URL="../Class/Search QSFP28 Fanout Gen2/Search QSFP28 Fanout Gen2.lvclass"/>
			</Item>
			<Item Name="Search QSFP10 Fanout" Type="Folder">
				<Item Name="Search QSFP10 Fanout.lvclass" Type="LVClass" URL="../Class/Search QSFP10 Fanout/Search QSFP10 Fanout.lvclass"/>
			</Item>
			<Item Name="Check Product ID" Type="Folder">
				<Item Name="Check Product ID.lvclass" Type="LVClass" URL="../Class/Check Product ID/Check Product ID.lvclass"/>
			</Item>
			<Item Name="Set Station" Type="Folder">
				<Item Name="Set Station.lvclass" Type="LVClass" URL="../Class/Set Station/Set Station.lvclass"/>
			</Item>
			<Item Name="Check Station" Type="Folder">
				<Item Name="Check Station.lvclass" Type="LVClass" URL="../Class/Check Station/Check Station.lvclass"/>
			</Item>
			<Item Name="Error Code" Type="Folder">
				<Item Name="Error Code.lvclass" Type="LVClass" URL="../Class/Error Code/Error Code.lvclass"/>
			</Item>
			<Item Name="Close USB-I2C" Type="Folder">
				<Item Name="Close USB-I2C.lvclass" Type="LVClass" URL="../Class/Close USB-I2C/Close USB-I2C.lvclass"/>
			</Item>
			<Item Name="Select Product" Type="Folder">
				<Item Name="Select Product.lvclass" Type="LVClass" URL="../Class/Select Product/Select Product.lvclass"/>
			</Item>
			<Item Name="Password" Type="Folder">
				<Item Name="Password.lvclass" Type="LVClass" URL="../Class/Password/Password.lvclass"/>
			</Item>
			<Item Name="Verify Lot Code By PN" Type="Folder">
				<Item Name="Verify Lot Code By PN.lvclass" Type="LVClass" URL="../Class/Verify Lot Code By PN/Verify Lot Code By PN.lvclass"/>
			</Item>
			<Item Name="Offset Current Temp" Type="Folder">
				<Item Name="Offset Current Temp.lvclass" Type="LVClass" URL="../Class/Offset Current Temp/Offset Current Temp.lvclass"/>
			</Item>
			<Item Name="Set Default LUT" Type="Folder">
				<Item Name="Set Default LUT.lvclass" Type="LVClass" URL="../Class/Set Default LUT/Set Default LUT.lvclass"/>
				<Item Name="Get LUT Temp Setting And Check Exist.vi" Type="VI" URL="../Class/Set Default LUT/Get LUT Temp Setting And Check Exist.vi"/>
			</Item>
			<Item Name="Verify Default LUT" Type="Folder">
				<Item Name="Verify Default LUT.lvclass" Type="LVClass" URL="../Class/Verify Default LUT/Verify Default LUT.lvclass"/>
			</Item>
			<Item Name="RSSI" Type="Folder">
				<Item Name="Get Pre Test RSSI" Type="Folder">
					<Item Name="Get Pre Test RSSI.lvclass" Type="LVClass" URL="../Class/Get Pre Test RSSI/Get Pre Test RSSI.lvclass"/>
				</Item>
				<Item Name="Compare RSSI" Type="Folder">
					<Item Name="Compare RSSI.lvclass" Type="LVClass" URL="../Class/Compare RSSI/Compare RSSI.lvclass"/>
					<Item Name="Tester.vi" Type="VI" URL="../Class/Compare RSSI/Tester.vi"/>
				</Item>
			</Item>
			<Item Name="Verify Difference Code" Type="Folder">
				<Item Name="Verify Difference Code.lvclass" Type="LVClass" URL="../Class/Verify Difference Code/Verify Difference Code.lvclass"/>
			</Item>
			<Item Name="Write Upper Page" Type="Folder">
				<Item Name="Write Upper Page.lvclass" Type="LVClass" URL="../Class/Write Upper Page.lvclass"/>
			</Item>
		</Item>
		<Item Name="Firmware Update Class" Type="Folder">
			<Item Name="Firmware Update" Type="Folder">
				<Item Name="Firmware Update.lvclass" Type="LVClass" URL="../Class/Firmware Update/Firmware Update.lvclass"/>
			</Item>
			<Item Name="RSSI" Type="Folder">
				<Item Name="RSSI.lvclass" Type="LVClass" URL="../Class/RSSI/RSSI.lvclass"/>
			</Item>
			<Item Name="Checksum" Type="Folder">
				<Item Name="Checksum.lvclass" Type="LVClass" URL="../Class/Checksum/Checksum.lvclass"/>
			</Item>
		</Item>
		<Item Name="TRx Test Class" Type="Folder">
			<Item Name="Target Temp" Type="Folder">
				<Item Name="Target Temp.lvclass" Type="LVClass" URL="../Class/Target Temp/Target Temp.lvclass"/>
			</Item>
			<Item Name="Get Voltage" Type="Folder">
				<Item Name="Get Voltage.lvclass" Type="LVClass" URL="../Class/Get Voltage/Get Voltage.lvclass"/>
			</Item>
			<Item Name="TxP Calibration" Type="Folder">
				<Item Name="TxP Calibration.lvclass" Type="LVClass" URL="../Class/TxP Calibration/TxP Calibration.lvclass"/>
			</Item>
			<Item Name="RxP Calibration" Type="Folder">
				<Item Name="RxP Calibration.lvclass" Type="LVClass" URL="../Class/RxP Calibration/RxP Calibration.lvclass"/>
			</Item>
			<Item Name="Tx Bias Calibration" Type="Folder">
				<Item Name="Tx Bias Calibration.lvclass" Type="LVClass" URL="../Class/Tx Bias Calibration/Tx Bias Calibration.lvclass"/>
			</Item>
			<Item Name="Get Reliability RSSI" Type="Folder">
				<Item Name="Get Reliability RSSI.lvclass" Type="LVClass" URL="../Class/Get Reliability RSSI/Get Reliability RSSI.lvclass"/>
			</Item>
			<Item Name="Get 40G Fanout Lot Number" Type="Folder">
				<Item Name="Get 40G Fanout Lot Number.lvclass" Type="LVClass" URL="../Class/Get 40G Fanout Lot Number/Get 40G Fanout Lot Number.lvclass"/>
			</Item>
		</Item>
		<Item Name="EEPROM Class" Type="Folder">
			<Item Name="By Work Order" Type="Folder">
				<Item Name="EEPROM Update By Work Order" Type="Folder">
					<Item Name="EEPROM Update By Work Order.lvclass" Type="LVClass" URL="../Class/EEPROM Update By Work Order/EEPROM Update By Work Order.lvclass"/>
				</Item>
				<Item Name="EEPROM Check By Work Order" Type="Folder">
					<Item Name="EEPROM Check By Work Order.lvclass" Type="LVClass" URL="../Class/EEPROM Check By Work Order/EEPROM Check By Work Order.lvclass"/>
				</Item>
			</Item>
			<Item Name="Check PN Type" Type="Folder">
				<Item Name="Check PN Type.lvclass" Type="LVClass" URL="../Class/Check PN Type/Check PN Type.lvclass"/>
			</Item>
			<Item Name="EEPROM Update" Type="Folder">
				<Item Name="Class" Type="Folder">
					<Item Name="Luxshare-ICT" Type="Folder">
						<Item Name="Luxshare-ICT.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Luxshare-ICT/Luxshare-ICT.lvclass"/>
					</Item>
					<Item Name="VIPS" Type="Folder">
						<Item Name="VIPS.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/VIPS/VIPS.lvclass"/>
					</Item>
					<Item Name="Not Define" Type="Folder">
						<Item Name="Not Define.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Not Define/Not Define.lvclass"/>
					</Item>
					<Item Name="Dell Force10" Type="Folder">
						<Item Name="Dell.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Dell/Dell.lvclass"/>
					</Item>
					<Item Name="Dell EMC" Type="Folder">
						<Item Name="Dell EMC.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Dell EMC/Dell EMC.lvclass"/>
					</Item>
					<Item Name="Cray" Type="Folder">
						<Item Name="Cray.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Cray/Cray.lvclass"/>
					</Item>
					<Item Name="Arista" Type="Folder">
						<Item Name="Arista.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Arista/Arista.lvclass"/>
					</Item>
					<Item Name="Pactech" Type="Folder">
						<Item Name="Pactech.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Pactech/Pactech.lvclass"/>
					</Item>
					<Item Name="Not Use" Type="Folder">
						<Item Name="Baycom" Type="Folder">
							<Item Name="Baycom.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Baycom/Baycom.lvclass"/>
						</Item>
					</Item>
					<Item Name="UBNT" Type="Folder">
						<Item Name="UBNT.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/UBNT/UBNT.lvclass"/>
					</Item>
					<Item Name="Silux" Type="Folder">
						<Item Name="Silux.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Silux/Silux.lvclass"/>
					</Item>
					<Item Name="Wavesplitter" Type="Folder">
						<Item Name="Wavesplitter.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Wavesplitter/Wavesplitter.lvclass"/>
					</Item>
					<Item Name="TRUSTNUO" Type="Folder">
						<Item Name="TRUSTNUO.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/TRUSTNUO/TRUSTNUO.lvclass"/>
					</Item>
					<Item Name="Light Express" Type="Folder">
						<Item Name="Light Express.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Light Express/Light Express.lvclass"/>
					</Item>
					<Item Name="Netgear" Type="Folder">
						<Item Name="Netgear.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Netgear/Netgear.lvclass"/>
					</Item>
					<Item Name="Panduit" Type="Folder">
						<Item Name="Panduit.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Panduit/Panduit.lvclass"/>
					</Item>
					<Item Name="Gigamon" Type="Folder">
						<Item Name="Gigamon.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Gigamon/Gigamon.lvclass"/>
					</Item>
					<Item Name="Ruijie" Type="Folder">
						<Item Name="Ruijie.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Ruijie/Ruijie.lvclass"/>
					</Item>
					<Item Name="Super Micro" Type="Folder">
						<Item Name="Super Micro.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Super Micro/Super Micro.lvclass"/>
					</Item>
					<Item Name="NOVASTAR" Type="Folder">
						<Item Name="NOVASTAR.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/NOVASTAR/NOVASTAR.lvclass"/>
					</Item>
					<Item Name="NOKIA" Type="Folder">
						<Item Name="NOKIA.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/NOKIA/NOKIA.lvclass"/>
					</Item>
					<Item Name="CORETEK" Type="Folder">
						<Item Name="CORETEK.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/CORETEK/CORETEK.lvclass"/>
					</Item>
					<Item Name="Connpro" Type="Folder">
						<Item Name="Connpro.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/Connpro/Connpro.lvclass"/>
					</Item>
					<Item Name="ALLRAY" Type="Folder">
						<Item Name="ALLRAY.lvclass" Type="LVClass" URL="../Class/EEPROM Update/Class/ALLRAY/ALLRAY.lvclass"/>
					</Item>
				</Item>
				<Item Name="EEPROM Update.lvclass" Type="LVClass" URL="../Class/EEPROM Update/EEPROM Update.lvclass"/>
			</Item>
			<Item Name="EEPROM Check" Type="Folder">
				<Item Name="EEPROM Check.lvclass" Type="LVClass" URL="../Class/EEPROM Check/EEPROM Check.lvclass"/>
			</Item>
			<Item Name="Version" Type="Folder">
				<Item Name="Version.lvclass" Type="LVClass" URL="../Class/Version/Version.lvclass"/>
			</Item>
			<Item Name="Threshold Check" Type="Folder">
				<Item Name="Threshold Check.lvclass" Type="LVClass" URL="../Class/Threshold Check/Threshold Check.lvclass"/>
			</Item>
			<Item Name="Set Customer Password" Type="Folder">
				<Item Name="Set Customer Password.lvclass" Type="LVClass" URL="../Class/Set Customer Password/Set Customer Password.lvclass"/>
			</Item>
			<Item Name="Write Temp Default" Type="Folder">
				<Item Name="Write Temp Default.lvclass" Type="LVClass" URL="../Class/Write Temp Default/Write Temp Default.lvclass"/>
			</Item>
			<Item Name="Check Temp Default" Type="Folder">
				<Item Name="Check Temp Default.lvclass" Type="LVClass" URL="../Class/Check Temp Default/Check Temp Default.lvclass"/>
			</Item>
			<Item Name="Reset Temp Default" Type="Folder">
				<Item Name="Reset Temp Default.lvclass" Type="LVClass" URL="../Class/Reset Temp Default/Reset Temp Default.lvclass"/>
			</Item>
			<Item Name="Get Product Temp Define" Type="Folder">
				<Item Name="Get Product Temp Define.lvclass" Type="LVClass" URL="../Class/Get Product Temp Define/Get Product Temp Define.lvclass"/>
			</Item>
			<Item Name="Verify PN Temp Define" Type="Folder">
				<Item Name="Verify PN Temp Define.lvclass" Type="LVClass" URL="../Class/Verify PN Temp Define/Verify PN Temp Define.lvclass"/>
			</Item>
			<Item Name="Update Page01" Type="Folder">
				<Item Name="Update Page01.lvclass" Type="LVClass" URL="../Class/Update Page01/Update Page01.lvclass"/>
			</Item>
			<Item Name="Calculate Arista CRC32" Type="Folder">
				<Item Name="Calculate Arista CRC32.lvclass" Type="LVClass" URL="../Class/Calculate Arista CRC32/Calculate Arista CRC32.lvclass"/>
			</Item>
			<Item Name="SN Replace" Type="Folder">
				<Item Name="SN Replace.lvclass" Type="LVClass" URL="../Class/SN Replace/SN Replace.lvclass"/>
			</Item>
		</Item>
		<Item Name="OQC Check" Type="Folder">
			<Item Name="OQC Check.lvclass" Type="LVClass" URL="../Class/OQC Check/OQC Check.lvclass"/>
		</Item>
		<Item Name="Conversion" Type="Folder">
			<Item Name="Conversion.lvclass" Type="LVClass" URL="../Class/Conversion/Conversion.lvclass"/>
		</Item>
		<Item Name="Monitor" Type="Folder">
			<Item Name="Monitor.lvclass" Type="LVClass" URL="../Class/Monitor/Monitor.lvclass"/>
		</Item>
		<Item Name="FQC Check" Type="Folder">
			<Item Name="Read Temp.lvclass" Type="LVClass" URL="../Class/Read Temp/Read Temp.lvclass"/>
		</Item>
		<Item Name="Outsourcing" Type="Folder">
			<Item Name="Serach OutSourcing Product" Type="Folder">
				<Item Name="Search OutSourcing Product.lvclass" Type="LVClass" URL="../Class/Search Outsourcing  Product/Search OutSourcing Product.lvclass"/>
			</Item>
			<Item Name="Write Outsourcing Password" Type="Folder">
				<Item Name="Write Outsourcing Password.lvclass" Type="LVClass" URL="../Class/Write Outscourcing Password/Write Outsourcing Password.lvclass"/>
			</Item>
			<Item Name="OutSourcing Get Lot Number" Type="Folder">
				<Item Name="OutSourcing Get Lot Number.lvclass" Type="LVClass" URL="../Class/OutSourcing Lot Number/OutSourcing Get Lot Number.lvclass"/>
			</Item>
			<Item Name="OutSourcing Write Lot Number" Type="Folder">
				<Item Name="OutSourcing Write Lot Number.lvclass" Type="LVClass" URL="../Class/OutSourcing Write Lot Number/OutSourcing Write Lot Number.lvclass"/>
			</Item>
			<Item Name="OutSourcing Check FW Version" Type="Folder">
				<Item Name="OutSourcing Check FW Version.lvclass" Type="LVClass" URL="../Class/OutSourcing Check FW Version/OutSourcing Check FW Version.lvclass"/>
			</Item>
			<Item Name="DDMI" Type="Folder">
				<Item Name="Calibration" Type="Folder">
					<Item Name="OutSourciing Set Temp Offset" Type="Folder">
						<Item Name="OutSourcing Set Temp Offset.lvclass" Type="LVClass" URL="../Class/OutSourcing Set Temp Offset/OutSourcing Set Temp Offset.lvclass"/>
					</Item>
					<Item Name="OutSourcing Cali Rx Power" Type="Folder">
						<Item Name="OutSourcing Cali Rx Power.lvclass" Type="LVClass" URL="../Class/OutSourcing Cali Rx Power/OutSourcing Cali Rx Power.lvclass"/>
					</Item>
					<Item Name="OutSourcing Cail Tx Power" Type="Folder">
						<Item Name="OutSourcing Cali Tx Power.lvclass" Type="LVClass" URL="../Class/OutSourcing Cali Tx Power/OutSourcing Cali Tx Power.lvclass"/>
					</Item>
				</Item>
				<Item Name="RSSI" Type="Folder">
					<Item Name="OutSourcing Check RSSI" Type="Folder">
						<Item Name="OutSourcing Check RSSI.lvclass" Type="LVClass" URL="../Class/OutSourcing Check RSSI/OutSourcing Check RSSI.lvclass"/>
					</Item>
					<Item Name="OutSourcing Set Burn in Mode" Type="Folder">
						<Item Name="OutSourcing Set Burn in Mode.lvclass" Type="LVClass" URL="../Class/OutSourcing Set Burn in Mode/OutSourcing Set Burn in Mode.lvclass"/>
					</Item>
					<Item Name="OutSourcing Set Burn in Current" Type="Folder">
						<Item Name="Outsourcing Set Burn in Current.lvclass" Type="LVClass" URL="../Class/Outsourcing Set Burn in Current/Outsourcing Set Burn in Current.lvclass"/>
					</Item>
					<Item Name="Enable RSSI Read" Type="Folder">
						<Item Name="Enable RSSI Read.lvclass" Type="LVClass" URL="../Class/Enable RSSI Read/Enable RSSI Read.lvclass"/>
					</Item>
				</Item>
			</Item>
			<Item Name="Check A2 Checksum" Type="Folder">
				<Item Name="Check A2 Checksum.lvclass" Type="LVClass" URL="../Class/Check A2 Checksum/Check A2 Checksum.lvclass"/>
			</Item>
			<Item Name="Restore Password" Type="Folder">
				<Item Name="Restore Password.lvclass" Type="LVClass" URL="../Class/Restore Password/Restore Password.lvclass"/>
			</Item>
			<Item Name="Verify Auth" Type="Folder">
				<Item Name="Verify Auth.lvclass" Type="LVClass" URL="../Class/Verify Auth/Verify Auth.lvclass"/>
			</Item>
			<Item Name="Verify No Password" Type="Folder">
				<Item Name="Verify No Password.lvclass" Type="LVClass" URL="../Class/Verify No Password/Verify No Password.lvclass"/>
			</Item>
		</Item>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CellBG is Red.vi" Type="VI" URL="../Public/CellBG is Red.vi"/>
		<Item Name="Get Baycom Serial Number.vi" Type="VI" URL="../SubVIs/Get Baycom Serial Number.vi"/>
		<Item Name="Get Custom Type.vi" Type="VI" URL="../SubVIs/Get Custom Type.vi"/>
		<Item Name="Get Product Type From Part Number.vi" Type="VI" URL="../SubVIs/Get Product Type From Part Number.vi"/>
		<Item Name="Is Customer PN.vi" Type="VI" URL="../SubVIs/Is Customer PN.vi"/>
	</Item>
	<Item Name="AOC.lvclass" Type="LVClass" URL="../AOC.lvclass"/>
</Library>
